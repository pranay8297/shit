from django.urls import path

from feed import views
app_name = "feed"

urlpatterns = [
    path('', views.PostList.as_view(), name='post-list'),
    path('upload/', views.upload_post, name='upload-post'),
    path('<int:pk>/', views.PostDetail.as_view(), name='post-detail'),
    path('list-api/', views.PostListView.as_view(), name='post-list-api'),
]  
