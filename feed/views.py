
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from django.shortcuts import render, redirect
from rest_framework import generics
from django.template.context_processors import csrf
from django.views.generic import CreateView, ListView, UpdateView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin

from feed.models import Post, Image, Comment
from feed.forms import PostForm, ImageForm, CommentForm
from feed.serializers import PostSerializer, ImageSerializer, CommentSerializer


class PostListView(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


def upload_post(request):
    template_name = 'feed/post_upload.html'
    if request.method == "POST":
        post_form = PostForm(request.POST)
        image_form = ImageForm(request.POST, request.FILES)
        # import ipdb
        # ipdb.set_trace()
        if post_form.is_valid() and image_form.is_valid():
            post = post_form.save(commit=False)
            post.user = request.user
            post.save()
            image = image_form.save(commit=False)
            image.post_id = post
            image.save()
        return redirect('feed:post-list')
    else:
        post_form = PostForm()
        image_form = ImageForm()

    args = {}
    args.update(csrf(request))
    args['post_form'] = post_form
    args['image_form'] = image_form

    return render(request, template_name, args)


class PostList(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'feed/post_list.html'

    def get(self, request, format=None):
        posts = Post.objects.all()
        for post in posts:
            post.first_image = post.image.first()
        return Response({'posts': posts})


class PostDetail(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'feed/post_detail.html'

    def get(self, request, pk, format=None):
        queryset = Post.objects.get(id=self.kwargs['pk'])
        queryset_image = Image.objects.get(post_id_id=self.kwargs['pk'])
        queryset_comment = Comment.objects.filter(post_id_id=self.kwargs['pk'])
        comment_count = len(queryset_comment)
        comment_form = CommentForm()

        args = {'posts': queryset,
                'images': queryset_image,
                'comments': queryset_comment,
                'comment_count': comment_count,
                'comment_form': comment_form,
                }
        args.update(csrf(request))
        return Response(args)

    def post(self, request, *args, **kwargs):
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.user = request.user
            post_obj = Post.objects.get(pk=kwargs['pk'])
            comment.post_id = post_obj
            comment.save()
            key = kwargs['pk']
        return redirect('feed:post-detail', key)


def add_comment(request, *args, **kwargs):
    template_name = 'feed/post_detail.html'
    if request.method == "POST":
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            # import pdb
            # pdb.set_trace()
            comment = comment_form.save(commit=False)
            comment.user = request.user
            post_obj = Post.objects.get(pk=kwargs['pk'])
            comment.post_id = post_obj
            comment.save()
            key = kwargs['pk']
            # import pdb
            # pdb.set_trace()
        return redirect('feed:post-detail', key)
        # import pdb
        # pdb.set_trace()
    else:
        comment_form = CommentForm()
    args = {}
    args.update(csrf(request))
    args['comment_form'] = comment_form
    return render(request, template_name, args)
