from django.db import models
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType

from django.utils.translation import ugettext_lazy as _


# Create your models here.
class Post(models.Model):
    OPEN = "O"
    CLOSED = "C"
    DRAFT = "D"
    STATUS = (
        (OPEN, _("Open")),
        (CLOSED, _("Closed")),
        (DRAFT, _("Draft")),
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    title = models.CharField(max_length=200, unique=True, blank=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=1, choices=STATUS, default=DRAFT)
    content = models.TextField(max_length=3000)

    def __str__(self):
        return self.title


class Image(models.Model):
    post_id = models.ForeignKey(
        Post, on_delete=models.CASCADE, related_name='image')
    image = models.ImageField(upload_to='images/')


class Comment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    post_id = models.ForeignKey(
        Post, on_delete=models.CASCADE, related_name='post')
    comment_content = models.CharField(max_length=400)
