from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Search

from elasticsearch import Elasticsearch

from pets import models


es = Elasticsearch(
    ['https://elastic:fQCwA3QsWGGfFXuq9Dr3mQ5Z@ca80fada02624f1a902f3e4db7b27504.europe-west1.gcp.cloud.es.io:9243'],

)

connections.create_connection(
    hosts=['https://elastic:fQCwA3QsWGGfFXuq9Dr3mQ5Z@ca80fada02624f1a902f3e4db7b27504.europe-west1.gcp.cloud.es.io:9243'], timeout=20)


class PetIndex(DocType):
    pet_name = Text()
    breed = Text()

    class Index:
        name = 'peto'


def bulk_indexing():

    for b in models.Pet.objects.all():
        b.indexing()


def elastic_search(query):
    s = Search(index='peto').query("query_string",
                                   query='*'+query+'*', fields=['pet_name', 'breed'])
    response = s.execute()
    set_of_id = set()
    for i in response:
        set_of_id.add(i['breed'])
    return set_of_id
