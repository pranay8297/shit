# serializers

from pets.models import Pet, Image
from rest_framework import serializers


class GetAllImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('image',)


class PetSerializer(serializers.ModelSerializer):
    image = GetAllImageSerializer(many=True, read_only=True)

    class Meta:
        model = Pet
        fields = ('id', 'pet_name', 'breed', 'gender', 'age', 'image')


class PetDetailSerializer(serializers.ModelSerializer):
    image = GetAllImageSerializer(many=True, read_only=True)

    class Meta:
        model = Pet
        fields = ('id', 'pet_name', 'breed', 'gender', 'age', 'image')


# class PetDetailSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = Pet
