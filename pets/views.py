
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from django.shortcuts import render
from user.models import Profile
from django.shortcuts import redirect
from rest_framework import generics
from django.template.context_processors import csrf
from django.views.generic import CreateView, ListView, UpdateView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework import permissions
from django.forms import modelformset_factory
from django.http import HttpRequest
from django.db.models import Q

from pets.models import Pet, Image
from pets.search import elastic_search
from pets.serializers import GetAllImageSerializer, PetSerializer
from pets.forms import PetForm, ImageForm
from pets.permissions import IsUserOrReadOnly
from math import sin, cos, sqrt, atan2, radians
import random

# dummy lat and long values for testing
list_of_locations = [[16.166700, 74.833298], [26.850000, 80.949997],
                     [28.610001, 77.230003], [19.076090, 72.877426],
                     [14.167040, 75.040298], [26.540457, 88.719391], ]

# from pets.models import Pet, Image


def calculate_distance(lat1, lon1, lat2, lon2):
    import pdb; pdb.set_trace()
    R = 6373.0
    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = R * c
    return distance


# generates a list of pets
class PetList(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'pets/pet_list.html'

    def get(self, request, format=None):
        import pdb; pdb.set_trace()

        id = request.user.id
        person = Profile.objects.get(id=id)
        lat1 = person.latitude
        lon1 = person.longitude

        pets = Pet.objects.filter(is_adopted=False)
        for pet in pets:
            lat2 = pet.latitude
            lon2 = pet.longitude
            distance = calculate_distance(lat1, lon1, lat2, lon2)
            pet.first_image = pet.image.first()
            pet.distance = round(distance)
        return Response({'pets': pets})


# gets details of the selected pet
class PetDetails(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'pets/pet_details.html'

    def get(self, request, pk, format=None):
        id = request.user.id
        person = Profile.objects.get(id=id)
        lat1 = person.latitude
        lon1 = person.longitude
        queryset = Pet.objects.get(id=self.kwargs['pk'])
        queryset_image = Image.objects.filter(pet_id_id=self.kwargs['pk'])
        number_image = len(queryset_image)
        lat2 = queryset.latitude
        lon2 = queryset.longitude
        distance = calculate_distance(lat1, lon1, lat2, lon2)
        queryset.distance = round(distance)
        return Response({'pet': queryset, 'images': queryset_image,
                         'n': number_image})


# class PetListView(generics.ListAPIView):
#     queryset = Pet.objects.all()
#     serializer_class = PetSerializer

#     def perform_create(self, serializer):
#         serializer.save(user=self.request.user)

# class PetDetailView(generics.RetrieveAPIView):
#     queryset = Pet.objects.all()
#     serializer_class = PetSerializer


class PetUpdateView(generics.UpdateAPIView):
    queryset = Pet.objects.all()
    serializer_class = PetSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsUserOrReadOnly)


class PetDeleteView(generics.DestroyAPIView):
    queryset = Pet.objects.all()
    serializer_class = PetSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsUserOrReadOnly)

# function to get the locaiton of the user while uploading the pets


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


# class based view to to upload a pet to into the database
def PetCreateView(request):
    template_name = 'pets/pet_upload.html'
    # change extra depending upon how many images you want
    image_form_set = modelformset_factory(Image, form=ImageForm, extra=2)
    if request.method == 'POST':
        pet_form = PetForm(request.POST)
        image_form = image_form_set(
            request.POST, request.FILES, queryset=Image.objects.none())
        if pet_form.is_valid() and image_form.is_valid():
            pet = pet_form.save(commit=False)
            pet.user = request.user
            # while deploying the project to get ip address
            client_ip = get_client_ip(request)
            pet.longitude = random.choice(list_of_locations)[1]
            pet.latitude = random.choice(list_of_locations)[0]
            pet.save()

            for single_form in image_form.cleaned_data:
                single_image = single_form['image']
                image_object = Image(image=single_image, pet_id=pet)
                image_object.save()
        import pdb; pdb.set_trace()
        return redirect("pets:pet-list")

    else:
        pet_form = PetForm()
        iformset = image_form_set(queryset=Image.objects.none())

    args = {}
    args.update(csrf(request))
    args['pet_form'] = pet_form
    args['image_form_set'] = iformset
    return render(request, template_name, args)


def set_pet_adopted(request, *args, **kwargs):
    pet = Pet.objects.get(pk=kwargs['pk'])
    if pet.user == request.user:
        pet.is_adopted = True
        pet.save()
    return redirect("pets:adopted-pets")
    # retun to the same page


class MyAdoptedPetsView(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'pets/adopted_pets.html'

    def get(self, request, format=None):
        pets = Pet.objects.filter(user=request.user)
        for pet in pets:
            pet.first_image = pet.image.first()

        return Response({'pets': pets})


# elasticsearch for searching pets through breed
def search(request):

    template_name = 'pets/pet_list.html'

    query = request.GET.get('q', '')
    # code for normal search
    # pets = Pet.objects.filter(Q(pet_name__icontains=query) | 
    #                           Q(breed__icontains=query))
    pets = Pet.objects.filter(breed__in=elastic_search(query))
    for pet in pets:
        pet.first_image = pet.image.first()
    return render(
        request, template_name, {'pets': pets})
