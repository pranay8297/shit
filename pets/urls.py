from django.urls import path

from pets import views
app_name = "pets"
urlpatterns = [
    path('pets/', views.PetList.as_view(), name='pet-list'),
    path('pets/<int:pk>/', views.PetDetails.as_view(), name='pet-details'),
    path('pets/upload', views.PetCreateView, name='image_upload'),
    path('pets/update/<int:pk>', views.PetUpdateView.as_view(),
         name='pet_update'),
    path('pets/delete/<int:pk>', views.PetDeleteView.as_view(),
         name='pet_delete'),
    path('pets/adopted-pets/<int:pk>', views.set_pet_adopted,
         name='pet_adopted'),
    path('pets/adopted-pets/', views.MyAdoptedPetsView.as_view(),
         name='adopted-pets'),
    
    path('pets/search/', views.search, name='search'),
]
