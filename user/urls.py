from django.conf.urls import url
from . import views

app_name = 'user'
urlpatterns = [
    url(r'signup/$', views.signup, name='signup'),
    url(r'login/$', views.loginView, name='login'),
    url(r'home/$', views.homeView, name='home'),
    url(r'logout/$', views.logout_view, name='logout_view'),
    url(r'profile/$', views.ProfileView.as_view(), name='profile')
]
