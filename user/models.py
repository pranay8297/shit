
# Create your models here.
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.conf import settings


class Profile(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    phone_number = models.CharField(
        null=False, blank=False, unique=True, max_length=12)
    bio = models.CharField(max_length=300)
    longitude = models.DecimalField(max_digits=8, decimal_places=3, null=True)
    latitude = models.DecimalField(max_digits=8, decimal_places=3, null=True)
