
# Create your models here.
from django.db import models
from django.urls import reverse
from pets.models import Pet, Image
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class Chat(models.Model):
    pet_id = models.ForeignKey(Pet, on_delete=models.CASCADE)
    sender = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="sender",
        on_delete=models.CASCADE)
    reciever = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="reciever",
        on_delete=models.CASCADE)
    is_authenticated = models.BooleanField(default=False)


class Message(models.Model):
    chat_id = models.ForeignKey(Chat, on_delete=models.CASCADE)
    messanger = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="messanger",
        on_delete=models.CASCADE)
    message_body = models.CharField(max_length=1000)

# class Deal(models.Models):
#     user = models.ForeignKey(User)
#     chat_id = models.ForeignKey(Chat)
#     pet_id = models.ForeignKey(Pet)
